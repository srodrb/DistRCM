/*
 * =====================================================================================
 *
 *       Filename:  rcm.c
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22/08/17 10:34:24
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#include "rcm.h"
int* rcm_interface(int n,int nnz, int* rowptr, int *colptr, int vertex){
    int *degree=(int*)malloc(n*sizeof(int)); 
    int* Permut;
    sparse_Matrix *A=sparse_Matrix_alloc(n, nnz);
    Boolean_Matrix *AA;
    
    for(int i=0; i<n+1; i++)
            A->indptr[i]=rowptr[i];
    for(int i=0; i<nnz; i++){
            A->colid[i]=colptr[i];
            A->aij[i]=1;}
            
    AA=Create_Laplacian(A);     
    AA=FIND_SIMETRIC_MATRIX(AA);//   problemas con esta funcion
    for(int i=0; i<A->n; i++)
        degree[i]=Calculate_Degree_Node(AA,i);
    //int r=FIND_PSEUDO_PERIPHERAL_VERTEX(AA,degree, vertex);    
    Permut= RCM_Algorithm(AA, degree);
     
    //como hemos empezado con 1 la permutacion, si queremos que la permutacion esté escrita con el 0 hemos de restar 1 a todo.
    
    
    for(int i=0; i<A->n; i++)
        printf(" %d ", Permut[i]);
    printf("\n");
     
    free(degree);
    sparse_Matrix_free(A);
    Boolean_Matrix_free(AA);
return Permut;
}
int* RCM_Algorithm(Boolean_Matrix *A, int *D){
        int *R=(int*)malloc(A->n *sizeof(int)); //Dense vector that stores ordering of all vertices
        int *Lnext;
        map *Rnext;
        int nv=0, contador=0;
        sparse_Vector *Lcur;
        int vertex=0, count=0, r;
        
        for(int i=0; i<A->n; i++)
                R[i]=-1;
        do{
        nv++;
        r=FIND_PSEUDO_PERIPHERAL_VERTEX(A,D, vertex);  
        printf("Periherial node: %d\n", r);
        Lcur=sparse_vector_alloc(1);
        Lcur->posiciones[0]=r;
        Lcur->etiquetas[0]=nv;
        
        R[r]=nv;
        contador=1;
            while(contador!=0){
                Lnext=SPMSPV(A, Lcur);//va bien
                Lnext=SELECT(Lnext, R, A->n);
                Rnext=SORTPERM(Lnext, D, A->n, nv);
                nv+=(Rnext->dim);
                
                SET2(R, Rnext);
                sparse_vector_free(Lcur);
                Lcur=sparse_vector_alloc(Rnext->dim);
                int cont=0; 
                for(int i=0; i<Rnext->dim; i++){
                        Lcur->posiciones[cont]=Rnext->PosOld[i];
                        Lcur->etiquetas[cont++]=Rnext->PosNew[i];
                }
            /*    printf("Lnext");
                for(int i=0; i<A->n; i++)
                        printf(" %d ", Lnext[i]);
                printf("\n");
                printf("R");
                for(int i=0; i<A->n; i++)
                        printf(" %d ", R[i]);
                printf("\n");
                 printf("Rnext, old");
                for(int i=0; i<Rnext->dim; i++)
                        printf(" %d ", Rnext->PosOld[i]);
                printf("\n");
                printf("Rnext, new");
                for(int i=0; i<Rnext->dim; i++)
                        printf(" %d ", Rnext->PosNew[i]);
                printf("\n");
                 printf("Lcur, posiciones");
                for(int i=0; i<Lcur->dimension; i++)
                        printf(" %d ", Lcur->posiciones[i]);
                printf("\n");
                 printf("Lcur, etiquetas");
                for(int i=0; i<Lcur->dimension; i++)
                        printf(" %d ", Lcur->etiquetas[i]);
                printf("\n"); */
                contador=Rnext->dim;
                free(Lnext);
                map_free(Rnext);
        }
        count=0;
        for(int i=0; i<A->n; i++)
                if(R[i]==-1){
                        count=-1;
                        vertex=i;
                        break;}
        }while(count==-1);
        
        
        for(int i=0; i<A->n; i++)
            R[i]--;
        R=invertir_posicion_etiqueta_R(R, A->n);
        R=Reverse(R, A->n);
return R;
}
int *invertir_posicion_etiqueta_R(int *R, int n){
        int *Invertir=(int*)malloc(n*sizeof(int));
        
        for(int i=0; i<n; i++)
                Invertir[R[i]]=i;
        free(R);
return Invertir;
}
sparse_Vector* sparse_vector_alloc(int dim){
        sparse_Vector *v;
        v=(sparse_Vector*) malloc(sizeof(sparse_Vector));
        
        v->etiquetas=(int*)malloc(dim*sizeof(int));
        v->posiciones=(int*)malloc(dim*sizeof(int));
        v->dimension=dim;
return v;
}
void sparse_vector_free(sparse_Vector *v){
    free(v->posiciones);
    free(v->etiquetas);
    free(v);
}
int minimum_degree(int *x, int *Degrees, int n){
    int minimum=-1;
    int posMin=-1;
    int xi=-1;
    for(int i=0; i<n; i++){
            if(minimum==-1 && x[i]>0){
                    if(Degrees[i]>0){
                            minimum=Degrees[i];
                            posMin=i;
                            xi=x[i];
                    }
            }else if(minimum>0 &&x[i]>0){
                    if(x[i]<xi){
                        minimum=Degrees[i];
                        posMin=i;
                        xi=x[i];
                    }else if(x[i]==xi && Degrees[i]<minimum){
                            minimum=Degrees[i];
                            posMin=i;
                            xi=x[i];}
            }
    }
return posMin;
}
int NONZEROELEMENTS(int *v, int n){
        int contador=0;
        for(int i=0; i<n; i++)
            if(v[i]!=0)
                contador++;
return contador;
}  
int comprobacion_elemento_en_R(int *R, int dimR, int elemento){
        for(int i=0; i<dimR; i++)
                if(R[i]==elemento)
                    return 1;
return 0;
}
void SET1(sparse_Vector *Lcur, int *R){
    for(int i=0; i<Lcur->dimension; i++){
            int posicion=Lcur->posiciones[i];
            R[posicion]=Lcur->etiquetas[i];
    }
return;
}  
void SET2(int *R, map *Rnext){
    for(int i=0; i<Rnext->dim; i++)
           // R[Rnext->PosNew[i]]=Rnext->PosOld[i];
            R[Rnext->PosOld[i]]=Rnext->PosNew[i];
return;
}

int * SELECT(int *Lnext, int *R, int n){
    int *L=(int*)calloc(n,sizeof(int));
    
    for(int i=0; i<n; i++)
        if(R[i]==-1)
            L[i]=Lnext[i];
    free(Lnext);
return L;
}
map * SORTPERM(int *x, int *y, int n, int anterior){
    int ** Tumble=(int**)calloc(3,sizeof(int*));
    map *Permut;
    int dimSort=0;
    for(int i=0; i<3; i++)
            Tumble[i]=(int*)calloc(n,sizeof(int));
    
    for(int i=0; i<n; i++)
            if(x[i]!=0){
                Tumble[0][dimSort]=x[i];
                Tumble[1][dimSort]=y[i];
                Tumble[2][dimSort]=i;
                dimSort++;
            }
    Permut=SORTINCREASINGDEGREE(Tumble[0], Tumble[1], Tumble[2], dimSort, anterior);
    for(int i=0; i<3; i++)
            free(Tumble[i]);
    free(Tumble);
return Permut;
}
map * SORTINCREASINGDEGREE(int *x, int *y, int *indices, int dimSort, int anterior){
    map *sort=map_alloc(dimSort);
    for(int i=0; i<dimSort; i++)
            for(int j=i; j<dimSort; j++){
                    if(x[j]<x[i])//PermuteElements
                        PermuteElements(x, y, indices, i, j);
                    else if(x[i]==x[j] &&y[i]>y[j])
                        PermuteElements(x, y, indices, i, j);
            }
    for(int i=0; i<dimSort; i++){
            sort->PosOld[i]=indices[i];
            sort->PosNew[i]=anterior+i+1;
    } 
return sort;
}
map* map_alloc(int n){
    map *A=(map*)malloc(sizeof(map));
    A->PosOld=(int*)malloc(n*sizeof(int));
    A->PosNew=(int*)malloc(n*sizeof(int));
    A->dim=n;
return A;
}
void map_free(map* A){
    free(A->PosOld);
    free(A->PosNew);
    free(A);
}
void PermuteElements(int *x, int *y, int *z, int pos1, int pos2){
        int aux;
        aux=x[pos1];
        x[pos1]=x[pos2];
        x[pos2]=aux;
        aux=y[pos1];
        y[pos1]=y[pos2];
        y[pos2]=aux;
        aux=z[pos1];
        z[pos1]=z[pos2];
        z[pos2]=aux;
}
int *SPMSPV(Boolean_Matrix *A, sparse_Vector *v){
        int* mul=(int*)calloc(A->n, sizeof(int));
        for(int row=0; row<A->n; row++)
                        mul[row]=find_mult_rowMatrix_Vector(A, row, v);
return mul;
}        
int find_mult_rowMatrix_Vector(Boolean_Matrix *A, int row, sparse_Vector *v){
        int min=0;
        int number;
    for(int posicion=A->indptr[row]; posicion<A->indptr[row+1]; posicion++){
                number=result_Matrixrowcol_vector(A, posicion, v);
                if(min==0 &&number!=0)
                        min=number;
                else if(number>0 &&number<min)
                        min=number;
    }
return min;
}
int result_Matrixrowcol_vector(Boolean_Matrix *A, int posicion, sparse_Vector *v){
        int col=A->colid[posicion];
        
        for(int i=0; i<v->dimension; i++)
                if(col==v->posiciones[i])
                        return v->etiquetas[i];
return 0;
}  
int* Reverse(int *v, int dim){
        int * Reverse = (int*) malloc(dim * sizeof(int));
        
        for(int i=0; i<dim; i++)
                Reverse[i]=v[dim-1-i];
        free(v);
return Reverse;
}
