#include <stdio.h>
#include <stdlib.h>

typedef struct{
        int Size;
        int end;
        int *array;
}Array;
typedef struct{
    int n, nnz;
    int * colid;
    int * indptr;
    double * aij;
} sparse_Matrix;
typedef struct{
        int n, nnz;
        int *colid;
        int *indptr;
} Boolean_Matrix;
typedef struct{
    int *etiquetas;
    int *posiciones;
    int dimension;
}sparse_Vector;
typedef struct{
    int *PosOld, *PosNew;
    int dim;
}map;

int FIND_PSEUDO_PERIPHERAL_VERTEX(Boolean_Matrix *, int *, int);
int REDUCE(int *, int *, int);
int REDUCE2(int *, int *, int);
int * SELECT(int *, int *, int);
int *SPMSPV(Boolean_Matrix *, sparse_Vector *);
void SET1(sparse_Vector *, int *);
void SET2(int *, int *, int);
void SET3(int *, int *, int);
void sparse_Matrix_free (sparse_Matrix *);
void Boolean_Matrix_free(Boolean_Matrix *);
Boolean_Matrix * FIND_SIMETRIC_MATRIX(Boolean_Matrix *);
int Calculate_Degree_Node(Boolean_Matrix *, int);
Boolean_Matrix* Create_Laplacian(sparse_Matrix *);
sparse_Matrix * read_sparse_Matrix_MM( FILE*, char[]);
sparse_Vector* sparse_vector_alloc(int);
void sparse_vector_free(sparse_Vector *);

/*
int main(void){
     char name[50]="AdjacencyMatrix2.c";
     FILE *pf;
     sparse_Matrix *A; Boolean_Matrix *AA;
     int *D;
     
     A=read_sparse_Matrix_MM(pf, name);
    
     AA=Create_Laplacian(A);//todo ok
     AA=FIND_SIMETRIC_MATRIX(AA);
     
     D=(int*)malloc(A->n * sizeof(int));
     
     for(int i=0; i<A->n; i++)
            D[i]=Calculate_Degree_Node(AA,i);
          
     int r=FIND_PSEUDO_PERIPHERAL_VERTEX(AA, D);
     
    
            printf(" r= %d \n", r);
            
     
     free(D);
     sparse_Matrix_free(A);
     Boolean_Matrix_free(AA);
     
return 0;
} 
*/

int FIND_PSEUDO_PERIPHERAL_VERTEX(Boolean_Matrix *A, int *D, int r){
    int l=0, nlvl=l-1;
    int AuxSuma;
    int AuxAnterior;
    int rs=r;
    
    int *L=(int*)malloc(A->n *sizeof(int));
    int *Lnext, *ComprobaR=(int*)calloc(A->n, sizeof(int));
    sparse_Vector *Lcur;
    
    ComprobaR[r]=1;
    while(l>nlvl){
            for(int i=0; i<A->n; i++)
                L[i]=-1;
            Lcur=sparse_vector_alloc(1);
            Lcur->posiciones[0]=r;
            Lcur->etiquetas[0]=0;
            
            AuxAnterior=l-nlvl;
            nlvl=l;
            L[r]=1;
        
            do{
                SET1(Lcur, L);//coje en Lcur los elementos de L no visitados
                for(int i=0; i<Lcur->dimension; i++)
                        Lcur->etiquetas[i] +=1;
                Lnext=SPMSPV(A, Lcur);//busca adyacientes
                Lnext=SELECT(Lnext, L, A->n);//quita los adyacientes ya visitados
                
                AuxSuma=0;
                for(int i=0; i<A->n; i++)
                    if(Lnext[i]!=0)
                        AuxSuma++;
                sparse_vector_free(Lcur);
                sparse_vector_alloc(AuxSuma);
                if(AuxSuma!=0){
                        SET3(L, Lnext, A->n);//poner en L los elementos de Lnext
                        int contador=0;
                        for(int i=0; i<A->n; i++)
                            if(Lnext[i]!=0){
                                    Lcur->posiciones[contador]=i;
                                    Lcur->etiquetas[contador++]=Lnext[i];
                                    Lcur->dimension=contador;
                            }
                }
                l+=1;
                /* for(int i=0; i<A->n; i++)
                        if(Lnext[i]!=0)
                                L[i]=l-nlvl; //guarda los niveles en L 
                                */
           /*     printf("Lcur, Pos");
                for(int i=0; i<Lcur->dimension; i++)
                        printf(" %d ", Lcur->posiciones[i]);
                printf("\n");
                printf("Lcur, Etiquetas");
                for(int i=0; i<Lcur->dimension; i++)
                        printf(" %d ", Lcur->etiquetas[i]);
                printf("\n");
                printf("Lnext");
                for(int i=0; i<A->n; i++)
                        printf(" %d ", Lnext[i]);
                printf("\n");
                printf("L");
                for(int i=0; i<A->n; i++)
                        printf(" %d ", L[i]);
                printf("\n");*/
            }while(AuxSuma!=0);
            rs=r;
            r=REDUCE(L, D, A->n);//pone en L los grados minimos
            if(ComprobaR[r]==1){
                l=0; nlvl=0;}
            ComprobaR[r]=1;
            
    }
    free(ComprobaR);
    sparse_vector_free(Lcur); free(Lnext); free(L);
return rs;
}
int REDUCE(int *L, int * D, int n1){//Lcur is the level in with the nodes are
    int min;
    int r;
    int posMax=0, max=0;
    
    //Buscar el maximo distancia hasta nodo r
    for(int i=0; i<n1; i++)
            if(L[i]>max){
                    posMax=i; 
                    max=L[i];
            }
    r=posMax;
    min=D[posMax];
    //Buscar el minimo de los L con distancia maxima
    for(int i=0; i<n1; i++)
            if(L[i]==max && D[i]<min){
                    r=i;
                    min=D[i];
            }
return r;
}  
void SET3(int *L, int *Lnext, int n){
        for(int i=0; i<n; i++)
                if(Lnext[i]!=0)
                        if(L[i]==-1)
                                L[i]=Lnext[i];
}
