/*
 * =====================================================================================
 *
 *       Filename:  rcm.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  22/08/17 10:34:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */
#ifndef _RCM_H_
#define _RCM_H_
#include <stdlib.h>
#include <stdio.h>

typedef struct{
        int Size;
        int end;
        int *array;
}Array;
typedef struct{
    int n, nnz;
    int * colid;
    int * indptr;
    double * aij;
} sparse_Matrix;
typedef struct{
    int n, nnz;
    int *colid;
    int *indptr;
} Boolean_Matrix;
typedef struct{
    int *etiquetas;
    int *posiciones;
    int dimension;
}sparse_Vector;
typedef struct{
    int *PosOld, *PosNew;
    int dim;
}map;

void PermuteElements(int *, int *, int *, int, int);
map * SORTINCREASINGDEGREE(int *, int *, int *, int, int);
int* Reverse(int *, int);
map * SORTPERM(int *, int *, int, int);
int *SPMSPV(Boolean_Matrix *, sparse_Vector *);
int * SELECT(int *, int *, int);
void SET1(sparse_Vector *, int *);
void SET2(int *, map *);
int NONZEROELEMENTS(int *, int);
int Calculate_Degree_Node(Boolean_Matrix *, int);
void sparse_Matrix_free (sparse_Matrix *);
void Boolean_Matrix_free(Boolean_Matrix *);
int* RCM_Algorithm(Boolean_Matrix *, int *);
Boolean_Matrix * FIND_SIMETRIC_MATRIX(Boolean_Matrix *);
Boolean_Matrix* Create_Laplacian(sparse_Matrix *);
sparse_Matrix * read_sparse_Matrix_MM( FILE*, char[]);
Array * array_alloc(void);
void array_push(Array*, int);
int FIND_PSEUDO_PERIPHERAL_VERTEX(Boolean_Matrix *, int *, int);
int* rcm_interface(int,int, int*, int *, int);
sparse_Matrix * sparse_Matrix_alloc(int, int);
int result_Matrixrowcol_vector(Boolean_Matrix *, int, sparse_Vector *);
int find_mult_rowMatrix_Vector(Boolean_Matrix *, int, sparse_Vector *);
void map_free(map*);
map* map_alloc(int);
sparse_Vector* sparse_vector_alloc(int);
void sparse_vector_free(sparse_Vector *);
int *invertir_posicion_etiqueta_R(int *, int);


#endif /*  end of RCM clause */
