#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef int QueueElement;
typedef struct{
	int size;
	int first;
	int last;
	QueueElement *data;
}Queue;
typedef struct{
        int Size;
        int end;
        int *array;
}Array;
typedef struct{
    int n, nnz;
    int * colid;
    int * indptr;
    double * aij;
} sparse_Matrix;
typedef struct{
        int n, nnz;
        int *colid;
        int *indptr;
} Boolean_Matrix;
    
Boolean_Matrix * Boolean_Matrix_alloc(int, int);
void Boolean_Matrix_free(Boolean_Matrix *);
int ADD_SPARSE_MATRIXES(sparse_Matrix *, sparse_Matrix *);
int MULTIPLY_SPARSE_MATRIX_TIMES_MATRIX(sparse_Matrix *, sparse_Matrix *);
void permute_SPARCEMATRIX(sparse_Matrix *, Array *);
void ordering2(int *, int *, double *, int);
Array * array_alloc2(int);
void sparse_Matrix_free (sparse_Matrix *);
sparse_Matrix * sparse_Matrix_alloc(int, int);
int give_position_knowing_row_col(sparse_Matrix *, int, int);
int give_position_knowing_row_col2(Boolean_Matrix *, int, int);
int is_there_ElementNonZero_in_row_colum(sparse_Matrix *, int, int);
int is_there_ElementNonZero_in_row_colum2(Boolean_Matrix *, int,int);
void ordering(int *, int *, int);


/*
int main(void){
    sparce_Matrix *A;
    Boolean_Matrix *AA;
    int *vector;
    
    
    A=read_sparce_Matrix();
    printf("Inicial matrix\n");
    print_sparce_Matrix_in_matrix_Form(A);
    
    vector=place_0_in_diagonal(A);
    
    printf("After taking elements from diagonal\n");
    print_sparce_Matrix_in_matrix_Form(A); printf("\n");
    
    AA=Create_Laplacian(A);//todo ok
    AA=FIND_SIMETRIC_MATRIX(AA);
    printf("Symmetric matrix\n");
    print_Boolean_Matrix_in_matrix_Form(A2);
    
return 0;
}
*/
/*
int BinarySearch(sparce_Matrix*A, int inicio, int final, int element){
        int medio=(inicio+final)/2; 
        
    if(final>=inicio){
        if(element==A->colid[inicio])   return inicio;
        if(element==A->colid[final])    return final;
    
        if(element==A->colid[medio]){
            return medio;
        }else if(element<A->colid[medio]){
            return BinarySearch(A, inicio, medio-1, element);
        }else if(element>A->colid[medio]){
            return BinarySearch(A, medio+1, final, element);
        }
    }
    else return -1;
}
*/

int BinarySearch(int* Array, int array_size, int value){
        int medida=array_size;
        int medio=array_size/2;
        
        if(medida>1){
            if(Array[medio]==value) return medio;
            else if(Array[medio]<value)
                    return BinarySearch(&Array[medio+1], medida-medio, value);
            else
                    return BinarySearch(&Array[0], medida-medio, value);

        }
        else if(medida==1)
                if(Array[0]==value) return medio;
        
    return -1;
}
Boolean_Matrix* Create_Laplacian(sparse_Matrix *A){
            Boolean_Matrix *B;
            int Count=0, col, diagPos;
            
            B=Boolean_Matrix_alloc(A->n, A->nnz);
            
            B->indptr[0]=0;
            
            if(A->n<200){
                for(int row=0; row<A->n; row++){
                    for(int position=A->indptr[row]; position<A->indptr[row+1]; position++){
                                col=A->colid[position];
                                if(row!=col){
                                            B->colid[Count]=col;
                                            Count++;}
                    }
                    B->indptr[row+1]=Count;
                }
            }else{//with binary search
                for(int row=0; row<A->n; row++){
                    //Vector es el vector que va de posicion A->indptr[row] a A->indptr[row+1]
                        int size=(A->indptr[row+1])-(A->indptr[row]);
                        int index=BinarySearch(&(A->colid[A->indptr[row]]), size, row);
                                if(index>=0){
                                    memcpy(&(B->colid[Count]),&(A->colid[A->indptr[row]]), index*sizeof(int));
                                    memcpy(&(B->colid[Count+index]),&(A->colid[A->indptr[row]+index+1]),(size-index)*sizeof(int));
                                    Count+=size-1;//modificar
                                }else{
                                        memcpy(&(B->colid[Count]), &(A->colid[A->indptr[row]]), size*sizeof(int));
                                        Count+=size;
                                }
                B->indptr[row+1]=Count;
                }
            }
            B->nnz=Count;
return B;
}
Boolean_Matrix * FIND_SIMETRIC_MATRIX(Boolean_Matrix *A){
        Boolean_Matrix *B;
        int contador;
        int * coo_col, * coo_row, *nnz_per_rows;//nnz_per_rows es numero de elementos por fila en traspuesta---- en este caso no sirve de nada
        int *col_simetric, *row_simetric;
        
        coo_col=(int*)malloc(A->nnz *sizeof(int));
        coo_row=(int*)malloc(A->nnz *sizeof(int));
        nnz_per_rows=(int*)malloc(A->n *sizeof(int));
        
        for(int i=0; i<A->n; i++)
                nnz_per_rows[i]=0;
        
        for(int row=0; row<A->n; row++)//guardar en formato COO
                    for(int i=A->indptr[row]; i<A->indptr[row+1]; i++){
                                coo_row[i]=row;
                                coo_col[i]=A->colid[i];
                                nnz_per_rows[A->colid[i]]++;//guardar cuantos elementos por fila de traspuesta=nº elementos columna A
                    }
        //Si queremos simetrizar juntaremos los vectores columna en col_sim y vectores fila en row_simetric
        col_simetric=(int*)malloc(2*A->nnz*sizeof(int));
        row_simetric=(int*)malloc(2*A->nnz*sizeof(int));
        
        for(int i=0; i<A->nnz; i++){
                col_simetric[i]=coo_col[i];
                row_simetric[i]=coo_row[i];
        }
        for(int i=A->nnz; i<2*A->nnz; i++){
                col_simetric[i]=coo_row[i-(A->nnz)];
                row_simetric[i]=coo_col[i-(A->nnz)];
        }
        free(coo_col); free(coo_row);
        ordering(row_simetric, col_simetric, 2*A->nnz);//ordenar el vector simetrico
        
        coo_row=(int*)calloc(A->n, sizeof(int));
        
        contador=0;
        //Ahora hace falta quitar los que estan repetidos.
        for(int i=0; i<2*A->nnz; i++){
                if(row_simetric[i]==row_simetric[i+1]){
                         if(col_simetric[i]==col_simetric[i+1]){
                             contador++;
                         }else{
                             coo_row[row_simetric[i]]+=1;}
                }else{
                    coo_row[row_simetric[i]]+=1;
                }
        }
        
        B=Boolean_Matrix_alloc(A->n, 2*(A->nnz)-contador);
        
        contador=0;
        for(int i=0; i<2*A->nnz-1; i++){
                if(row_simetric[i]==row_simetric[i+1]){
                         if(col_simetric[i]!=col_simetric[i+1]){
                             B->colid[contador]=col_simetric[i];
                             contador++;
                         }
                }else{
                    B->colid[contador]=col_simetric[i];
                    contador++;}
        }
        
        B->colid[(B->nnz)-1]=col_simetric[(2*A->nnz)-1];
        /*
        if(row_simetric[(2*A->nnz)-1]==row_simetric[(2*A->nnz)-2]){
                    if(col_simetric[(2*A->nnz)-1]!=col_simetric[(2*A->nnz)-2])//!= no estoy seguro que sea correcto
                            B->colid[(B->nnz)-1]=col_simetric[(2*A->nnz)-1];
        }else{
                    B->colid[(B->nnz)-1]=col_simetric[(2*A->nnz)-1];
        }
        */
        B->indptr[0]=0;
        for(int i=0; i<B->n; i++)   
                B->indptr[i+1]=coo_row[i]+B->indptr[i];
        Boolean_Matrix_free(A);
        free(coo_row);free(col_simetric); free(row_simetric); free(nnz_per_rows);
         
return B;
}
Boolean_Matrix * TriangularSup_Matrix(Boolean_Matrix *A){//A tiene que tener 0's en diagonal, sino esta funcion no vale
           int row, iCount=0;
           Boolean_Matrix *B;
           
           B=Boolean_Matrix_alloc(A->n, (A->nnz)/2);
           
           for(row=0; row<(A->n); row++){
               for(int j=A->indptr[row]; j<A->indptr[row+1]; j++)
                            if(A->colid[j]>row){
                                    B->colid[j]=A->colid[j]; 
                                    iCount++;
                            }
                B->indptr[row+1]=iCount;
           }
           Boolean_Matrix_free(A);
return B;
}
int Calculate_Degree_Node(Boolean_Matrix *A, int row){//A ha de ser triang sup
    int degree=(A->indptr[row+1])-(A->indptr[row]);
return degree;
}
void ordering(int *row, int *col, int dim){
        int a;
        for(int j=0; j<dim; j++)
                for(int i=j+1; i<dim; i++){
                        if(row[i]<row[j]){
                                a=row[i];
                                row[i]=row[j];
                                row[j]=a;
                                a=col[i];
                                col[i]=col[j];
                                col[j]=a;
                        }
                        else if(row[i]==row[j]){
                                if(col[i]< col[j]){
                                    a=col[i];
                                    col[i]=col[j];
                                    col[j]=a;}
                        }
                }
return;
}
Boolean_Matrix * Boolean_Matrix_alloc(int dimMatrix, int NumberElements){
        Boolean_Matrix *A;
        A=(Boolean_Matrix*)malloc(sizeof(Boolean_Matrix));
        A->n=dimMatrix;
        A->nnz=NumberElements;
        
        A->colid=(int*)malloc(NumberElements*sizeof(int));
        A->indptr=(int*)malloc((dimMatrix+1)*sizeof(int));
return A;
} 
void Boolean_Matrix_free(Boolean_Matrix *A){
        free(A->colid);
        free(A->indptr);
} 
void print_Boolean_Matrix_in_matrix_Form(Boolean_Matrix *A){
    int row,col, position;
        
        for(row=0; row<(A->n); row++){
                    for(col=0; col<(A->n); col++){
                            if(is_there_ElementNonZero_in_row_colum2(A, row, col)==1){//hay elemento nonZero en matriz
                                    position= give_position_knowing_row_col2(A, row, col);
                                    printf("\t%d", 1);
                            }else   //there is no element
                                printf("\t0");
                    }
                    printf("\n");
        }
return;
} 
void print_sparse_Matrix_in_matrix_Form(sparse_Matrix *A){
        int row,col, position;
        
        for(row=0; row<(A->n); row++){
                    for(col=0; col<(A->n); col++){
                            if(is_there_ElementNonZero_in_row_colum(A, row, col)==1){//hay elemento nonZero en matriz
                                    position= give_position_knowing_row_col(A, row, col);
                                    printf("\t%f", A->aij[position]);
                            }else   //there is no element
                                printf("\t0");
                    }
                    printf("\n");
        }
return;
} 
void erase_element_in_sparseMatrix2(sparse_Matrix *A, int row, int col){
        sparse_Matrix *B;
        int position, ElementRow, i;
        
        B=sparse_Matrix_alloc(A->n, (A->nnz)-1);
            
        position=give_position_knowing_row_col(A, row, col);
        if(position==-1) {printf("Cannot find the element in row, col\n"); exit(1);} //This will not happen because there exists an element nonZero in position row col
        
        for(i=0; i<position; i++){
                B->aij[i]=A->aij[i];
                B->colid[i]=A->colid[i];}
                
        for(i=position +1; i<A->nnz; i++){
                B->aij[i-1]=A->aij[i];
                B->colid[i-1]=A->colid[i];}
        
        for(i=0; i<=row; i++)
                B->indptr[i]=A->indptr[i];
        for(i=row+1; i<(A->n)+1; i++)
                B->indptr[i]=(A->indptr[i])-1;
        
        sparse_Matrix_free(A);
        
        A=sparse_Matrix_alloc(B->n, B->nnz);
        
        for(i=0; i<A->nnz; i++){
            A->aij[i]=B->aij[i];
            A->colid[i]=B->colid[i];
        }
        for(i=0; i< (A->n)+1; i++)
                A->indptr[i]=B->indptr[i];
        
        sparse_Matrix_free(B);
return;
}
int find_element_Matrix_knowing_row_colum( sparse_Matrix *A, int row, int col){
            int ElementRow; //These are the number of elements that are in the row 
            int i;
            
            ElementRow=(A->indptr[row+1])-(A->indptr[row]);
            
            for(i=0; i<ElementRow; i++)
                    if(col==(A->colid[(A->indptr[row])+i]))
                            return (A->aij[(A->indptr[row]+i)]);
                    
return 0;
}
int find_element_Matrix_knowing_row_colum2( Boolean_Matrix *A, int row, int col){
            int ElementRow; //These are the number of elements that are in the row 
            int i;
            
            ElementRow=(A->indptr[row+1])-(A->indptr[row]);
            
            for(i=0; i<ElementRow; i++)
                    if(col==(A->colid[(A->indptr[row])+i]))
                            return 1;
                    
return 0;
}
int * place_0_in_diagonal(sparse_Matrix *A){//devuelvo un vector con los elementos que ha suprimido
        int i, position;
        int *vector;
        vector=(int*)malloc((A->n)*sizeof(int));
        
        for(i=0; i<(A->n); i++){
                if(find_element_Matrix_knowing_row_colum(A,i,i)!=0){//solo hago este paso si el elemento en diagonal no es 0
                        position=give_position_knowing_row_col(A, i,i);
                        vector[i]=(A->aij[position]);
                        erase_element_in_sparseMatrix2(A,i,i);}
                else vector[i]=0;
        }
return vector;
}
void replace_elements_in_diagonal(sparse_Matrix *A, int * vector, int *Diagonal, int NonZero){
        int dim, NNZ;
        int *coo_col, *coo_row;
        double *coo_num;
    
        coo_col=(int*)malloc((A->nnz +NonZero) *sizeof(int));
        coo_row=(int*)malloc((A->nnz +NonZero) *sizeof(int));
        coo_num=(double*)malloc((A->nnz +NonZero) *sizeof(double));
        
        for(int row=0; row<A->n; row++)
                    for(int i=A->indptr[row]; i<A->indptr[row+1]; i++){
                                coo_row[i]=row;
                                coo_col[i]=A->colid[i];
                                coo_num[i]=A->aij[i];
                    }

        for(int i=0; i<NonZero; i++){
                    coo_row[i+A->nnz]=Diagonal[i];
                    coo_col[i+A->nnz]=Diagonal[i];
                    coo_num[i+A->nnz]=vector[Diagonal[i]];
        }
        ordering2(coo_row, coo_col,coo_num,A->nnz +NonZero);
        dim=A->n; NNZ=A->nnz+NonZero;
        sparse_Matrix_free(A);
        A=sparse_Matrix_alloc(dim, NNZ);
        
        for(int i=0; i<=A->n; i++)
                A->indptr[i]=0;
        
        for(int i=0; i<NNZ; i++){
                A->colid[i]=coo_col[i];
                A->aij[i]=coo_num[i];
                (A->indptr[coo_row[i]+1])+=1;
        }
        for(int i=0; i<A->n; i++)
                (A->indptr[i+1])+=(A->indptr[i]);
        free(coo_col); free(coo_row); free(coo_num);
return;
}
sparse_Matrix * read_sparse_Matrix (void){
            sparse_Matrix *A; FILE *pf;
            int dimMatrix, NumberElements, i;
            
            pf=fopen("AdjacencyMatrix2.c", "r");
            
           fscanf(pf, " %d", &dimMatrix);
           fscanf(pf, " %d", &NumberElements);
           
           A=sparse_Matrix_alloc(dimMatrix, NumberElements);
           
           for(i=0; i<NumberElements; i++)
                    fscanf(pf, " %lf", &A->aij[i]);
           
           for(i=0; i<NumberElements; i++)
                    fscanf(pf, " %d", &(A->colid[i]));
           for(i=0; i<dimMatrix +1; i++)
                    fscanf(pf, " %d", &(A->indptr[i]));
           
           fclose(pf);
return A;
}
sparse_Matrix * sparse_Matrix_alloc(int dimMatrix, int NumberElements){
        sparse_Matrix * A;
        A=(sparse_Matrix *) malloc(sizeof(sparse_Matrix));
        A->n=dimMatrix;
        A->nnz=NumberElements;
        
        A->colid=(int*)malloc(NumberElements *sizeof(int));
        A->indptr=(int*)malloc((dimMatrix+1)*sizeof(int));
        A->aij=(double*) malloc(NumberElements *sizeof(double));
return A;
}
void sparse_Matrix_free (sparse_Matrix * A){
        free(A->aij);
        free(A->indptr);
        free(A->colid);
        free(A);
}
int give_position_knowing_row_col(sparse_Matrix *A, int row, int col){
        int position, ElementRow, i;
        
        ElementRow=(A->indptr[row+1])-(A->indptr[row]);//Number of elements in the row
        
        for(i=0; i<ElementRow; i++)
                if(col==(A->colid[(A->indptr[row])+i])){
                        position=(A->indptr[row])+i;
                        return position;}
return -1;
}
int give_position_knowing_row_col2(Boolean_Matrix *A, int row, int col){
        int position, ElementRow, i;
        
        ElementRow=(A->indptr[row+1])-(A->indptr[row]);//Number of elements in the row
        
        for(i=0; i<ElementRow; i++)
                if(col==(A->colid[(A->indptr[row])+i])){
                        position=(A->indptr[row])+i;
                        return position;}
return -1;
}
int is_there_ElementNonZero_in_row_colum(sparse_Matrix *A, int row, int col){//this function seeks the element in the matrix position row col
        int ElementRow;
        ElementRow=(A->indptr[row+1])-(A->indptr[row]);
        
        int Aij;
        
        for(int i=0; i<ElementRow; i++)
                if(col==(A->colid[(A->indptr[row])+i]))
                        return 1;
return 0;
}
int is_there_ElementNonZero_in_row_colum2(Boolean_Matrix *A, int row, int col){//this function seeks the element in the matrix position row col
        int ElementRow;
        ElementRow=(A->indptr[row+1])-(A->indptr[row]);
        
        int Aij;
        
        for(int i=0; i<ElementRow; i++)
                if(col==(A->colid[(A->indptr[row])+i]))
                        return 1;
return 0;
}  
Array* find_inverse_permutation(Array *Permute){
        Array * InversePermutation;
        
        InversePermutation=array_alloc2(Permute->Size);
        
        for(int i=0; i<(Permute->end); i++)
                InversePermutation->array[Permute->array[i]]=i;
        
return InversePermutation;
} 
void ordering2(int *row, int *col, double *aij, int dim){
        int a;
        double b;
        for(int j=0; j<dim; j++)
                for(int i=j+1; i<dim; i++){
                        if(row[i]<row[j]){
                                a=row[i];
                                row[i]=row[j];
                                row[j]=a;
                                a=col[i];
                                col[i]=col[j];
                                col[j]=a;
                                b=aij[i];
                                aij[i]=aij[j]; 
                                aij[j]=b;
                        }else if(row[i]==row[j]){
                                if(col[i]< col[j]){
                                    a=col[i];
                                    col[i]=col[j];
                                    col[j]=a;
                                    b=aij[i];
                                    aij[i]=aij[j];
                                    aij[j]=b;
                                }
                        }
                }
return;
}
void permute_SPARCEMATRIX(sparse_Matrix *A, Array *Permute){
    int *col, *row, *col2, *row2;
    double *numb;
    
    numb=(double*)malloc((A->nnz)*sizeof(double));
    col=(int*)malloc((A->nnz)*sizeof(int));
    col2=(int*)malloc((A->nnz)*sizeof(int));
    row=(int*)malloc((A->nnz)*sizeof(int));
    row2=(int*)malloc((A->nnz)*sizeof(int));
    
    for(int j=0; j<A->n; j++)
          for(int i=A->indptr[j]; i<A->indptr[j+1]; i++){
                       row[i]=j;
                       row2[i]=j;
                       col[i]=A->colid[i];
                       col2[i]=A->colid[i];
                       numb[i]=A->aij[i];
         }
    //Permutar filas
    for(int i=0; i<A->nnz; i++){
            row[i]=Permute->array[row2[i]];
            col[i]=Permute->array[col2[i]];
    }
    
    free(col2); free(row2); 
    ordering2(row, col, numb, A->nnz);
    
    for(int i=0; i<=A->n; i++)
            A->indptr[i]=0;
    //Copy to the matrix A
    for(int i=0; i<(A->nnz); i++){
            A->aij[i]=numb[i];
            A->colid[i]=col[i];
            (A->indptr[row[i]+1])++;
    }
    
    for(int i=0; i<A->n; i++)
        (A->indptr[i+1])+=(A->indptr[i]);
    
return;
}
int * MULTIPLY_SPARSE_MATRIX_TIMES_VECTOR(sparse_Matrix *A, int *v, int dimVector){
        int *Result;
                
        Result=(int*)malloc(dimVector * sizeof(int));
        
        for(int row=0; row<dimVector; row++){
                Result[row]=0;
                for(int position=A->indptr[row]; position<A->indptr[row+1]; position++)
                        Result[row]+=(A->aij[position]*v[A->colid[position]]);
        }
        
return Result;
}

sparse_Matrix * read_sparse_Matrix_MM( FILE* pf, char name[]){
    int NumbRow, NumbCol, Elements, i=0;
    int *col, *row;
    double *elem;
    sparse_Matrix *A;
    char a[50]; double hola;
    
            pf=fopen(name, "r");
            if(pf==NULL){
                    printf("Doesn't find program\n");
                    exit(1);}
            /*
            while(i==0){import elements from python
                //fgets(a, sizeof(a), pf); 
                fscanf(pf, " %[\n]s ", &a);
                if(a[0]!='%'){
                        i=1;
                        //NumbRow=(int)a;
                }
            }
            */
            fscanf(pf, " %d", &NumbRow);
            fscanf(pf, " %d", &NumbCol);
            fscanf(pf, " %d", &Elements);
            
            elem=(double*)malloc(Elements*sizeof(double));
            col=(int*)malloc(Elements*sizeof(int));
            row=(int*)malloc(Elements*sizeof(int));
            
            for(i=0; i<Elements; i++){
                    fscanf(pf, " %d", &(row[i]));
                    fscanf(pf, " %d", &(col[i]));
                    fscanf(pf, " %lf", &elem[i]);
                    row[i]-=1;//le quito 1 para que empiece con 0
                    col[i]-=1;
            }
            
            ordering2(row, col, elem, Elements);
            A=sparse_Matrix_alloc(NumbRow, Elements);
            
            for(i=0; i<A->nnz; i++){
                    A->aij[i]=1; //elem[i];
                    A->colid[i]=col[i];
            }
            
            for(i=0;i<=A->n; i++)
                    A->indptr[i]=0;
            for(i=0; i<A->nnz; i++)
                    A->indptr[row[i]+1]+=1;
            for(i=0; i<A->n; i++)
                    A->indptr[i+1]+=A->indptr[i];
            free(elem); free(col); free(row);
            fclose(pf);
return A;
}
int return_position_in_COO_matrix(int *rowA, int *colA, int row, int col, int dim){
        int position=0;
        for(int i=0; i<dim; i++){
                if(rowA[i]==row)
                        if(colA[i]==col)
                                return position;
            position++;
        }
return -1;
}
int MULTIPLY_SPARSE_MATRIX_TIMES_MATRIX(sparse_Matrix *A, sparse_Matrix *B){//supongamos que las matrices son cuadradas
        int posicion=0, position1, position2, numb=0;
        double *elemA, *elemB, *elemC;
        int *rowA, *rowB, *rowC, *colA, *colB, *colC;
        
        if(A->n != B->n)
                return 0;
        
        elemA=(double*)malloc(A->nnz*sizeof(double));
        colA=(int*)malloc(A->nnz*sizeof(int));
        rowA=(int*)malloc(A->nnz*sizeof(int));
        
        elemB=(double*)malloc(B->nnz*sizeof(double));
        colB=(int*)malloc(B->nnz*sizeof(int));
        rowB=(int*)malloc(B->nnz*sizeof(int));
        
        elemC=(double*)malloc((A->nnz+B->nnz)*sizeof(double));
        colC=(int*)malloc((A->nnz+B->nnz)*sizeof(int));
        rowC=(int*)malloc((A->nnz+B->nnz)*sizeof(int));
        
        for(int j=0; j<A->n; j++)
            for(int i=A->indptr[j]; i<A->indptr[j+1]; i++){
                       rowA[i]=j;
                       colA[i]=A->colid[i];
                       elemA[i]=A->aij[i];
         }
         for(int j=0; j<B->n; j++)
            for(int i=B->indptr[j]; i<B->indptr[j+1]; i++){
                       rowB[i]=j;
                       colB[i]=B->colid[i];
                       elemB[i]=B->aij[i];
         }
        for(int i=0; i<A->n; i++)
                for(int j=0; j<B->n; j++){
                        numb=0;
                        for(int k=0; k<A->n; k++){
                            position1=return_position_in_COO_matrix(rowA, colA, i, k, A->nnz);
                            position2=return_position_in_COO_matrix(rowB, colB, k, j, B->nnz);
                            if(position1!=-1 && position2!=-1)
                                    numb+=elemA[position1]*elemB[position2];
                        }
                        if(numb!=0){
                            elemC[posicion]=numb; 
                            posicion++;
                            rowC[posicion]=i; 
                            colC[posicion]=j;}
                }
        ordering2(rowC, colC, elemC, posicion);
        //reconvertir C a csr
        sparse_Matrix_free(B);
        B=sparse_Matrix_alloc(A->n, posicion);
            
        for(int i=0; i<B->nnz; i++){
                B->aij[i]=elemC[i];
                B->colid[i]=colC[i];
        }
        for(int i=0;i<=B->n; i++)
                B->indptr[i]=0;
        for(int i=0; i<B->nnz; i++)
                B->indptr[rowC[i]+1]+=1;
        for(int i=0; i<B->n; i++)
                B->indptr[i+1]+=B->indptr[i];      
            
        free(elemA); free(elemB); free(elemC);
        free(rowA); free(rowB); free(rowC);
        free(colA); free(colB); free(colC);
        
return 1;
}
int ADD_SPARSE_MATRIXES(sparse_Matrix *A, sparse_Matrix *B){// suponemos que son cuadradas
        int posicion=0, comprovador=0;
        int *rowA, *rowB, *rowC, *colA, *colB, *colC;
        double *elemA, *elemB, *elemC;
        
        if(A->n != B->n)
                return 0;
        
        elemA=(double*)malloc(A->nnz*sizeof(double));
        colA=(int*)malloc(A->nnz*sizeof(int));
        rowA=(int*)malloc(A->nnz*sizeof(int));
        
        elemB=(double*)malloc(B->nnz*sizeof(double));
        colB=(int*)malloc(B->nnz*sizeof(int));
        rowB=(int*)malloc(B->nnz*sizeof(int));
        
        elemC=(double*)malloc((A->nnz+B->nnz)*sizeof(double));
        colC=(int*)malloc((A->nnz+B->nnz)*sizeof(int));
        rowC=(int*)malloc((A->nnz+B->nnz)*sizeof(int));
        
        for(int j=0; j<A->n; j++)
            for(int i=A->indptr[j]; i<A->indptr[j+1]; i++){
                       rowA[i]=j;
                       colA[i]=A->colid[i];
                       elemA[i]=A->aij[i];
                       rowC[i]=j;
                       colC[i]=A->colid[i];
                       elemC[i]=A->aij[i];
                       posicion++;
         }
         for(int j=0; j<B->n; j++)
            for(int i=B->indptr[j]; i<B->indptr[j+1]; i++){
                       rowB[i]=j;
                       colB[i]=B->colid[i];
                       elemB[i]=B->aij[i];
         }
         
         for(int i=0; i<B->nnz; i++){
                comprovador=0;
                for(int j=0; j<A->nnz; j++){
                        if(rowB[i]==rowA[j])
                                if(colB[i]==colA[j]){
                                        elemC[j]+=elemB[i]; 
                                        comprovador++;
                                }
                } 
                if(comprovador==0){
                        elemC[posicion]=elemB[i];
                        colC[posicion]=colB[i];
                        rowC[posicion]=rowB[i];
                        posicion++;
                }
         }
         
         ordering2(rowC, colC, elemC, posicion);
        //reconvertir C a csr
        sparse_Matrix_free(B);
        B=sparse_Matrix_alloc(A->n, posicion);
            
        for(int i=0; i<B->nnz; i++){
                B->aij[i]=elemC[i];
                B->colid[i]=colC[i];
        }
        for(int i=0;i<=B->n; i++)
                B->indptr[i]=0;
        for(int i=0; i<B->nnz; i++)
                B->indptr[rowC[i]+1]+=1;
        for(int i=0; i<B->n; i++)
                B->indptr[i+1]+=B->indptr[i];      
            
        free(elemA); free(elemB); free(elemC);
        free(rowA); free(rowB); free(rowC);
        free(colA); free(colB); free(colC);
return 1;
}

