#include <stdlib.h>

typedef int QueueElement;
typedef struct{
	int size;
	int first;
	int last;
	QueueElement *data;
}Queue;
typedef struct{
        int Size;
        int end;
        int *array;
}Array;

Array * array_alloc(void){
	Array * R = (Array*) malloc(sizeof(Array));
	R->Size = 1000;
	R->end  = 0;
	R->array = (int*) malloc( R->Size * sizeof(int));
	return (R);
}
Array * array_alloc2(int dimArray){
	Array * R = (Array*) malloc(sizeof(Array));
	R->Size = dimArray;
	R->end  = dimArray;
	R->array = (int*) malloc( dimArray * sizeof(int)); 
	return (R);
}
void array_free(Array* R){
	free(R->array);
	free(R);
return;
}
int array_is_full(Array* R){
	if ( R->end == R->Size ) return 1;
	else return 0;
}
void array_resize(Array *R){
    Array *NEW;
    
    NEW=array_alloc();
    NEW->Size=(R->Size)*2;
    (NEW->end) = (R->end);
    for(int i=0; i<NEW->end; i++)
            NEW->array[i]=R->array[i];
    array_free(R);
    
    R=(Array*) malloc(sizeof(Array));
    (R->Size) =(NEW->Size);
    (R->end) = (NEW->end);
    R->array= (int*) malloc(R->Size *sizeof(int));
    for(int i=0; i<(R->end); i++)
                R->array[i] = NEW->array[i];
    array_free(NEW);
return;
}
void array_push(Array* R, int E){
	/* check whether the queue is e..*/
    int i;
	if ( array_is_full(R)==1 )
		array_resize(R);
    
    R->array[R->end]=E;
    (R->end)+=1;
return;
}
int array_search(Array *R,int element){
    int i;
    
    for(i=0; i< R->end; i++)
                if(R->array[i] ==element)
                            return 1;//Esta en la lista
return 0;
}
void array_doubled_nodes(Array *R){//The array is already ordered
     int i,j, iCount=0;
     Array *NEW;
     
     NEW=array_alloc();
     
     //keep the first element of Array, the array will not be empty, it is because of the program, main
     array_push(NEW, R->array[0]);
     
     for(i=1; i<(R->end); i++){
                if((R->array[i])!=(NEW->array[iCount])){//si el elemento de array es diferente R es diferente a los ya guardados en NEW
                            array_push(NEW, R->array[i]);
                            iCount++;
                }
     }
     array_free(R);
     R=array_alloc();
     
     for(i=0; i<=iCount; i++)
            array_push(R, NEW->array[i]);
     array_free(NEW);
return;
}
