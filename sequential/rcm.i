%module rcm

%{
#include "rcm.h"
%}

%include "rcm.h"
%include "cpointer.i"
%pointer_functions(int, intptr)
%include "carrays.i"
%array_functions(int, cint)
